# -*- coding: utf-8 -*-
"""
    Flask-app integrates with frontend React and backend code for Value Drivers Application

    :copyright: (c) 2016 by Nikhil Pakki, India
    :license: MIT, see LICENSE for more details.
"""
from __future__ import annotations

import json
import os
import pandas as pd
from flask import Flask, request, send_file, Response
from flask_cors import cross_origin
from werkzeug.exceptions import HTTPException
import Value_Drivers as VD
import warnings
warnings.filterwarnings('ignore')

# creating Flask app
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = 'logs'
input_file_path = ''
df = pd.DataFrame()


def main() -> None:
    """
    Main function runs the application on a local development server
    :return: None
    """
    app.run(host="0.0.0.0", port=5000, threaded=True, debug=True)


@app.route('/')
def homepage() -> str:
    """
    Renders the home page with default message
    :return: string to be displayed on the home screen
    """
    return 'This is no longer a frontend interface. Please use <b>http://mysinergi.sanofi.com/</b> for value drivers'


@app.route('/download_template', methods=['GET', 'POST'])
@cross_origin()
def download_template() -> Response:
    """
    Download the generated sheet if form was submitted, else downloads the sample input template
    :return: generated/input_template file response based on the request method
    """
    cur_dir = os.path.dirname(os.path.realpath(__file__))
    input_template_file_path = os.path.join(cur_dir, 'input_template', 'INPUT_FILE_TEMPLATE.xlsx')
    value_drivers_path = os.path.join(cur_dir, 'Value_Drivers.xlsx')

    # download the generated sheet
    if request.method == 'POST':
        return send_file(value_drivers_path, as_attachment=True)

    # download template, if it wasn't submitted
    return send_file(input_template_file_path, as_attachment=True)


def get_parent_orgs(text: str = 'sanofi') -> list:
    """
    Gets the entire list of parent corporations
    :param text: Partial text which is used for filtering the parent corps.
    :return: list of parent corporations
    """
    global df
    parent_df = df[df['Corporation'].str.contains(text, regex=True, case=False)]['Corporation']
    return list(set(parent_df))


def get_orgs(org_list: list) -> list:
    """
    Gets the unique list of corporations
    :param org_list: List of all organizations
    :return: list of unique corporations
    """
    return list(set(org_list))


def get_entry_market_orgs(text: str = 'sanofi') -> list:
    """
    This function gets the Entry Market corporations
    :param text: parent org
    :return: list of entry market corporations
    """
    global df
    parent_categories = list(set(df[df['Corporation'].str.contains(text, regex=True, case=False)]['Category']))
    df_temp = df[df['Category'].isin(parent_categories)].copy()
    return list(set(df_temp['Corporation']))


@app.route('/get_entry_market', methods=['GET', 'POST'])
@cross_origin()
def get_entry_market() -> dict[str, list] | None:
    """
    Gets the real time entry market corps whenever the parent org is changed
    :return: dict with key 'ret_val' and value as the list of entry market corporations
    """
    global df
    if request.method == 'POST':
        text = request.json['text']
        parent_categories = list(set(df[df['Corporation'].str.contains(text, regex=True, case=False)]['Category']))
        df_temp = df[df['Category'].isin(parent_categories)].copy()
        return {'ret_val': list(set(df_temp['Corporation']))}


@app.route('/process', methods=['GET', 'POST'])
@cross_origin()
def process() -> dict[str, list[list[str]] | list | list]:
    """
    Processes the input file and populates the fields for user selection
    :return: process_page template on the UI
    """
    global input_file_path, df
    cur_dir = os.path.dirname(os.path.realpath(__file__))
    if request.method == 'POST':
        input_file = request.files['file']

        # get ip address and create a directory if not exists
        ip = request.access_route[0]
        ip_path = os.path.join(cur_dir, ip)
        if not os.path.exists(ip_path):
            os.mkdir(ip_path)

        # save the input file in {ip}
        input_file_path = os.path.join(ip_path, input_file.filename)
        input_file.save(input_file_path)

        # read the input file
        df = pd.read_excel(input_file_path, sheet_name='Sheet1')
        org_list = df['Corporation'].apply(lambda x: x.upper())

        # get all orgs list
        orgs = get_orgs(org_list)

        # get parent orgs
        parent_orgs = get_parent_orgs()

        # calculate non-parent org
        set_parent = set(parent_orgs)
        set_orgs = set(orgs)
        non_parent_orgs = sorted(set_orgs - set_parent)

        # global top 10
        global_top10 = list(VD.global_top10_tup)
        global_top10 = sorted(set_orgs & set(global_top10))

        # calculate non_global_top10
        non_global_top10 = sorted(set_orgs - set(global_top10))

        # assigning tags ('selected'/'') for each corporation
        chc_is_present = any(map(lambda x: 'chc' in x.lower(), parent_orgs))
        parent_orgs = sorted(map(lambda x: ['selected', x] if 'chc' in x.lower() else ['', x], parent_orgs))
        non_parent_orgs = sorted(map(lambda x: ['', x], non_parent_orgs))
        parent_selection = ''
        if len(parent_orgs):
            if not chc_is_present:
                parent_orgs[0][0] = 'selected'
                parent_selection = parent_orgs[0][1]
            else:
                for sel, corp in parent_orgs:
                    if sel == 'selected':
                        parent_selection = corp
                        break
        else:
            if len(non_parent_orgs):
                non_parent_orgs[0][0] = 'selected'
                parent_selection = non_parent_orgs[0][1]

        # get entry market orgs
        entry_market_orgs = get_entry_market_orgs(parent_selection)

        ret_args = {'parent_orgs': parent_orgs, 'non_parent_orgs': non_parent_orgs,
                    'global_top10': global_top10, 'non_global_top10': non_global_top10,
                    'entry_orgs': entry_market_orgs}
        return ret_args


@app.route('/generate', methods=['GET', 'POST'])
@cross_origin()
def generate() -> str | None:
    """
    Generate value drivers spreadsheet based on user selections
    :return: 'Pass' acknowledgement once the file is successfully generated
    """
    global input_file_path
    if request.method == 'POST':

        # get the ip address
        ip = request.access_route[0]

        # get parent corporation
        parent_corp_payload = request.form.get('parent_corp')

        # get the list of the global top 10 corporations
        global_top10_payload = request.form.getlist('global_top10')
        if len(global_top10_payload):
            global_top10_payload = (global_top10_payload[0]).split(',')

        # get the competitor corporation
        competitor_payload = request.form.get('competitor')

        # create an instance of ValueDriver
        driver_input = VD.ValueDriver(input_file_path, parent_corp_payload, global_top10=global_top10_payload,
                                      competitor_name=competitor_payload)

        # generate the Value Drivers report
        cur_dir = os.path.dirname(os.path.realpath(__file__))
        output_file_path = os.path.join(cur_dir, ip)
        driver_input.generate_report(output_path=output_file_path)

        print('File generation complete')

        return 'Pass'


@app.route('/download_file', methods=['GET', 'POST'])
@cross_origin()
def download_file():
    """
    Generate value drivers spreadsheet based on user selections
    :return: Value_Drivers output file as a download
    """
    ip = request.access_route[0]
    cur_dir = os.path.dirname(os.path.realpath(__file__))
    output_file_path = os.path.join(cur_dir, ip, 'Value_Drivers.xlsx')
    return send_file(output_file_path, as_attachment=True)


@app.route('/is_online', methods=['GET', 'POST'])
@cross_origin()
def is_online():
    """
    For frontend to ensure process is running
    :return: True
    """
    return 'Online'


@app.errorhandler(HTTPException)
@cross_origin()
def handle_exception(e: HTTPException) -> Response:
    """
    JSON instead of HTML for HTTP errors.
    :param e: HTTPException
    :return: JSON for exceptions
    """
    print(e)
    # start with the correct headers and status code from the error
    response = e.get_response()
    # replace the body with JSON
    response.data = json.dumps({
        "code": e.code,
        "name": e.name,
        "description": e.description,
    })
    response.content_type = "application/json"
    return response


if __name__ == '__main__':
    main()
