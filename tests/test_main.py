import io
import json
import os
from flask.testing import FlaskClient
from tests import VALID_STATUS_CODE
import pytest
import warnings
import main
from main import get_orgs
warnings.filterwarnings('ignore')


# @pytest.hookimpl(hookwrapper=True)
# def pytest_runtest_makereport(item, call):
#     outcome = yield
#     report = outcome.get_result()
#
#     test_fn = item.obj
#     docstring = getattr(test_fn, '__doc__')
#     if docstring:
#         report.nodeid = docstring


@pytest.mark.parametrize('a, b', [(get_orgs(['A', 'B', 'C']), ['A', 'B', 'C']),
                                  (get_orgs(['A', 'A', 'B']), ['A', 'B']),
                                  (get_orgs(['A', 'B', 'B']), ['A', 'B']),
                                  (get_orgs(['A', 'A', 'B', 'B']), ['A', 'B']),
                                  (get_orgs(['A', 'A', 'A']), ['A']),
                                  (get_orgs(['', '']), ['']),
                                  (get_orgs([1, 2, 4]), [1, 2, 4]),
                                  (get_orgs([1, 1, 4, 4]), [1, 4]),
                                  ])
def test_get_orgs(a: list, b: list[str] | list[int]) -> None:
    """
    Testing the get_orgs function
    :param a: get_orgs(list)
    :param b: list of string/int
    :return: None
    """
    assert set(a) == set(b)


@pytest.mark.homepage
def test_route_homepage(test_client: FlaskClient) -> None:
    """
    Testing the route '/' for the Value Driver app
    :param test_client: test client for this application
    :return: None
    """
    client = test_client
    response = client.get('/')
    expected_val = 'This is no longer a frontend interface. Please use ' \
                   '<b>http://mysinergi.sanofi.com/</b> for value drivers'
    assert response.status_code == VALID_STATUS_CODE
    assert response.data.decode('utf-8') == expected_val


@pytest.mark.download_template
def test_route_download_template_get(test_client: FlaskClient) -> None:
    """
    Testing the route '/download_template' for 'GET' request method for the Value Driver app
    :param test_client: test client for this application
    :return: None
    """
    client = test_client
    input_template_folder = 'input_template'
    input_template_filename = 'INPUT_FILE_TEMPLATE.xlsx'
    root_dir = os.path.dirname(os.getcwd())
    output_folder_path = os.path.join(root_dir, input_template_folder)
    output_file_path = os.path.join(output_folder_path, input_template_filename)
    if not os.path.exists(output_folder_path):
        os.mkdir(output_folder_path)
    if not os.path.exists(output_file_path):
        main.df.to_excel(output_file_path)
    response = client.get('/download_template')
    assert response.status_code == VALID_STATUS_CODE
    assert response.data


@pytest.mark.download_template
def test_route_download_template_post(test_client: FlaskClient) -> None:
    """
    Testing the route '/download_template' for 'POST' request method for the Value Driver app
    :param test_client: test client for this application
    :return: None
    """
    client = test_client
    input_template_filename = 'Value_Drivers.xlsx'
    root_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    output_folder_path = os.path.join(root_dir, input_template_filename)
    if not os.path.exists(output_folder_path):
        main.df.to_excel(output_folder_path)
    response = client.post('/download_template')
    assert response.status_code == VALID_STATUS_CODE
    assert response.data


@pytest.mark.get_entry_market
def test_route_get_entry_market_get(test_client: FlaskClient) -> None:
    """
    Testing the route '/get_entry_market' for 'GET' request method for the Value Driver app
    :param test_client: test client for this application
    :return: None
    """
    client = test_client
    response = client.get('/get_entry_market')
    assert response.status_code != VALID_STATUS_CODE
    # assert response.data


@pytest.mark.get_entry_market
def test_route_get_entry_market_post(test_client: FlaskClient) -> None:
    """
    Testing the route '/get_entry_market' for 'POST' request method for the Value Driver app
    :param test_client: test client for this application
    :return: None
    """
    client = test_client
    temp_df = main.df
    frame = [('CORP_1', 'CAT_1'),
             ('CORP_2', 'CAT_1'),
             ('CORP_1', 'CAT_2'),
             ('CORP_1', 'CAT_4'),
             ('CORP_3', 'CAT_3'),
             ('CORP_4', 'CAT_1'),
             ('CORP_4', 'CAT_2'),
             ('CORP_5', 'CAT_5'),
             ]
    d_frame = main.pd.DataFrame(frame, columns=['Corporation', 'Category'])
    main.df = d_frame
    response = client.post('/get_entry_market', data=json.dumps(dict(text='CORP_1')), content_type='application/json')
    assert response.status_code == VALID_STATUS_CODE
    assert set(response.json['ret_val']) == {'CORP_1', 'CORP_2', 'CORP_4'}
    main.df = temp_df


@pytest.mark.process(nodeid=1)
def test_route_process_1(test_client: FlaskClient) -> None:
    """
    Testing the route '/process' for the Value Driver app
    :param test_client: test client for this application
    :return: None
    """
    client = test_client
    # temp_df = main.df
    frame = [('CORP_1', 'CAT_1'),
             ('CORP_2', 'CAT_1'),
             ('CORP_1', 'CAT_2'),
             ('CORP_1', 'CAT_4'),
             ('CORP_3', 'CAT_3'),
             ('CORP_4', 'CAT_1'),
             ('CORP_4', 'CAT_2'),
             ('CORP_5', 'CAT_5'),
             ]
    d_frame = main.pd.DataFrame(frame, columns=['Corporation', 'Category'])
    main.df = d_frame
    towrite = io.BytesIO()
    d_frame.to_excel(towrite, index=False)
    data = dict(file=(io.BytesIO(towrite.getvalue()), "temp_file.xlsx"))
    response = client.post('/process', data=data, follow_redirects=True,
                           content_type='multipart/form-data')
    status_code = response.status_code
    ret_data = response.json

    # assert statements
    assert status_code == VALID_STATUS_CODE
    assert set(ret_data['parent_orgs']) == set()
    comps_list = ['CORP_1', 'CORP_2', 'CORP_3', 'CORP_4', 'CORP_5']
    comps_list = sorted(map(lambda x: ['', x], comps_list))
    comps_list[0][0] = 'selected'
    assert ret_data['non_parent_orgs'] == comps_list
    assert set(ret_data['global_top10']) == set()
    assert set(ret_data['non_global_top10']) == {'CORP_1', 'CORP_2', 'CORP_3', 'CORP_4', 'CORP_5'}
    assert set(ret_data['entry_orgs']) == {'CORP_1', 'CORP_2', 'CORP_4'}


@pytest.mark.process
def test_route_process_2(test_client: FlaskClient) -> None:
    """
    Testing the route '/process' for the Value Driver app
    :param test_client: test client for this application
    :return: None
    """
    client = test_client
    # temp_df = main.df
    frame = [('SANOFI_1', 'CAT_1'),
             ('CORP_2', 'CAT_1'),
             ('CORP_1', 'CAT_2'),
             ('CORP_1', 'CAT_4'),
             ('CORP_3', 'CAT_3'),
             ('CORP_4', 'CAT_1'),
             ('CORP_4', 'CAT_2'),
             ('CORP_5', 'CAT_5'),
             ]
    d_frame = main.pd.DataFrame(frame, columns=['Corporation', 'Category'])
    main.df = d_frame
    towrite = io.BytesIO()
    d_frame.to_excel(towrite, index=False)
    data = dict(file=(io.BytesIO(towrite.getvalue()), "temp_file.xlsx"))
    response = client.post('/process', data=data, follow_redirects=True,
                           content_type='multipart/form-data')
    status_code = response.status_code
    ret_data = response.json

    # assert statements
    assert status_code == VALID_STATUS_CODE
    assert ret_data['parent_orgs'] == [['selected', 'SANOFI_1']]
    comps_list = ['CORP_1', 'CORP_2', 'CORP_3', 'CORP_4', 'CORP_5']
    comps_list = sorted(map(lambda x: ['', x], comps_list))
    assert ret_data['non_parent_orgs'] == comps_list
    assert set(ret_data['global_top10']) == set()
    assert set(ret_data['non_global_top10']) == {'SANOFI_1', 'CORP_1', 'CORP_2', 'CORP_3', 'CORP_4', 'CORP_5'}
    assert set(ret_data['entry_orgs']) == {'SANOFI_1', 'CORP_2', 'CORP_4'}


@pytest.mark.process
def test_route_process_3(test_client: FlaskClient) -> None:
    """
    Testing the route '/process' for the Value Driver app
    :param test_client: test client for this application
    :return: None
    """
    client = test_client
    # temp_df = main.df
    frame = [('SANOFI_1', 'CAT_1'),
             ('CORP_2', 'CAT_1'),
             ('CORP_1', 'CAT_2'),
             ('SANOFI_2', 'CAT_2'),
             ('CORP_1', 'CAT_4'),
             ('CORP_3', 'CAT_3'),
             ('CORP_4', 'CAT_1'),
             ('CORP_4', 'CAT_2'),
             ('CORP_5', 'CAT_5'),
             ]
    d_frame = main.pd.DataFrame(frame, columns=['Corporation', 'Category'])
    main.df = d_frame
    towrite = io.BytesIO()
    d_frame.to_excel(towrite, index=False)
    data = dict(file=(io.BytesIO(towrite.getvalue()), "temp_file.xlsx"))
    response = client.post('/process', data=data, follow_redirects=True,
                           content_type='multipart/form-data')
    status_code = response.status_code
    ret_data = response.json

    # assert statements
    assert status_code == VALID_STATUS_CODE
    assert ret_data['parent_orgs'] == [['selected', 'SANOFI_1'], ['', 'SANOFI_2']]
    comps_list = ['CORP_1', 'CORP_2', 'CORP_3', 'CORP_4', 'CORP_5']
    comps_list = sorted(map(lambda x: ['', x], comps_list))
    assert ret_data['non_parent_orgs'] == comps_list
    assert set(ret_data['global_top10']) == set()
    assert set(ret_data['non_global_top10']) == {'SANOFI_1', 'SANOFI_2', 'CORP_1', 'CORP_2', 'CORP_3', 'CORP_4', 'CORP_5'}
    assert set(ret_data['entry_orgs']) == {'SANOFI_1', 'CORP_2', 'CORP_4'}


@pytest.mark.process
def test_route_process_4(test_client: FlaskClient) -> None:
    """
    Testing the route '/process' for the Value Driver app
    :param test_client: test client for this application
    :return: None
    """
    client = test_client
    # temp_df = main.df
    frame = [('SANOFI_1', 'CAT_1'),
             ('CORP_2', 'CAT_1'),
             ('CORP_1', 'CAT_2'),
             ('SANOFI_CHC_2', 'CAT_2'),
             ('CORP_1', 'CAT_4'),
             ('CORP_3', 'CAT_3'),
             ('CORP_4', 'CAT_1'),
             ('CORP_4', 'CAT_2'),
             ('CORP_5', 'CAT_5'),
             ]
    d_frame = main.pd.DataFrame(frame, columns=['Corporation', 'Category'])
    main.df = d_frame
    towrite = io.BytesIO()
    d_frame.to_excel(towrite, index=False)
    data = dict(file=(io.BytesIO(towrite.getvalue()), "temp_file.xlsx"))
    response = client.post('/process', data=data, follow_redirects=True,
                           content_type='multipart/form-data')
    status_code = response.status_code
    ret_data = response.json

    # assert statements
    assert status_code == VALID_STATUS_CODE
    assert ret_data['parent_orgs'] == [['', 'SANOFI_1'], ['selected', 'SANOFI_CHC_2']]
    comps_list = ['CORP_1', 'CORP_2', 'CORP_3', 'CORP_4', 'CORP_5']
    comps_list = sorted(map(lambda x: ['', x], comps_list))
    assert ret_data['non_parent_orgs'] == comps_list
    assert set(ret_data['global_top10']) == set()
    assert set(ret_data['non_global_top10']) == {'SANOFI_1', 'SANOFI_CHC_2', 'CORP_1', 'CORP_2', 'CORP_3', 'CORP_4', 'CORP_5'}
    assert set(ret_data['entry_orgs']) == {'SANOFI_CHC_2', 'CORP_1', 'CORP_4'}


@pytest.mark.process
def test_route_process_5(test_client: FlaskClient) -> None:
    """
    Testing the route '/process' for the Value Driver app
    :param test_client: test client for this application
    :return: None
    """
    client = test_client
    # temp_df = main.df
    frame = [('SANOFI_1', 'CAT_1'),
             ('CORP_2', 'CAT_1'),
             ('CORP_1', 'CAT_2'),
             ('SANOFI_CHC_2', 'CAT_2'),
             ('CORP_1', 'CAT_4'),
             ('CORP_3', 'CAT_3'),
             ('CORP_4', 'CAT_1'),
             ('CORP_4', 'CAT_2'),
             ('CORP_5', 'CAT_5'),
             ('NESTLE', 'CAT_5'),
             ('BAYER', 'CAT_1'),
             ]
    d_frame = main.pd.DataFrame(frame, columns=['Corporation', 'Category'])
    main.df = d_frame
    towrite = io.BytesIO()
    d_frame.to_excel(towrite, index=False)
    data = dict(file=(io.BytesIO(towrite.getvalue()), "temp_file.xlsx"))
    response = client.post('/process', data=data, follow_redirects=True,
                           content_type='multipart/form-data')
    status_code = response.status_code
    ret_data = response.json

    # assert statements
    assert status_code == VALID_STATUS_CODE
    assert ret_data['parent_orgs'] == [['', 'SANOFI_1'], ['selected', 'SANOFI_CHC_2']]
    comps_list = ['CORP_1', 'CORP_2', 'CORP_3', 'CORP_4', 'CORP_5', 'NESTLE', 'BAYER']
    comps_list = sorted(map(lambda x: ['', x], comps_list))
    assert ret_data['non_parent_orgs'] == comps_list
    assert set(ret_data['global_top10']) == {'NESTLE', 'BAYER'}
    assert set(ret_data['non_global_top10']) == {'SANOFI_1', 'SANOFI_CHC_2', 'CORP_1', 'CORP_2', 'CORP_3', 'CORP_4', 'CORP_5'}
    assert set(ret_data['entry_orgs']) == {'SANOFI_CHC_2', 'CORP_1', 'CORP_4'}


@pytest.mark.skip
def test_route_process_6(test_client: FlaskClient) -> None:
    """
    Testing the route '/process' for the Value Driver app
    :param test_client: test client for this application
    :return: None
    """
    client = test_client

    # delete storage folder
    root_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    ip = client.environ_base['REMOTE_ADDR']
    ip_folder_path = os.path.join(root_dir, ip)
    import shutil
    shutil.rmtree(ip_folder_path, ignore_errors=True)

    frame = [('SANOFI_1', 'CAT_1'),
             ('CORP_2', 'CAT_1'),
             ('CORP_1', 'CAT_3'),
             ('SANOFI_CHC_2', 'CAT_2'),
             ('CORP_1', 'CAT_4'),
             ('CORP_3', 'CAT_3'),
             ('CORP_4', 'CAT_1'),
             ('CORP_4', 'CAT_4'),
             ('CORP_5', 'CAT_5'),
             ]
    d_frame = main.pd.DataFrame(frame, columns=['Corporation', 'Category'])
    main.df = d_frame
    towrite = io.BytesIO()
    d_frame.to_excel(towrite, index=False)
    data = dict(file=(io.BytesIO(towrite.getvalue()), "temp_file.xlsx"))
    response = client.post('/process', data=data, follow_redirects=True,
                           content_type='multipart/form-data')
    status_code = response.status_code
    ret_data = response.json

    # assert statements
    assert status_code == VALID_STATUS_CODE
    assert ret_data['parent_orgs'] == [['', 'SANOFI_1'], ['selected', 'SANOFI_CHC_2']]
    comps_list = ['CORP_1', 'CORP_2', 'CORP_3', 'CORP_4', 'CORP_5']
    comps_list = sorted(map(lambda x: ['', x], comps_list))
    assert ret_data['non_parent_orgs'] == comps_list
    assert set(ret_data['global_top10']) == set()
    assert set(ret_data['non_global_top10']) == {'SANOFI_1', 'SANOFI_CHC_2', 'CORP_1', 'CORP_2', 'CORP_3', 'CORP_4', 'CORP_5'}
    assert set(ret_data['entry_orgs']) == {'SANOFI_CHC_2'}


@pytest.mark.generate
def test_route_generate_get(test_client: FlaskClient) -> None:
    """
    Testing the route '/generate' for the Value Driver app
    :param test_client: test client for this application
    :return: None
    """
    client = test_client
    response = client.get('/generate')
    assert response.status_code != VALID_STATUS_CODE


@pytest.mark.generate
def test_route_generate_post_1(test_client: FlaskClient) -> None:
    """
    Testing the route '/generate' for the Value Driver app
    :param test_client: test client for this application
    :return: None
    """
    client = test_client
    frame = [('SANOFI_1', 'CAT_1'),
             ('CORP_2', 'CAT_1'),
             ('CORP_1', 'CAT_2'),
             ('SANOFI_CHC_2', 'CAT_2'),
             ('CORP_1', 'CAT_4'),
             ('CORP_3', 'CAT_3'),
             ('CORP_4', 'CAT_1'),
             ('CORP_4', 'CAT_2'),
             ('CORP_5', 'CAT_5'),
             ('NESTLE', 'CAT_5'),
             ('BAYER', 'CAT_1'),
             ]
    d_frame = main.pd.DataFrame(frame, columns=['Corporation', 'Category'])
    d_frame['Country'] = 'IND'
    d_frame['Brand_Family'] = 'T'
    d_frame[['Val_PY', 'Val_CY', 'Vol_PY', 'Vol_CY']] = 1
    main.df = d_frame
    towrite = io.BytesIO()
    d_frame.to_excel(towrite, index=False)
    data = dict(file=(io.BytesIO(towrite.getvalue()), "temp_file.xlsx"))
    response = client.post('/process', data=data, follow_redirects=True,
                           content_type='multipart/form-data')
    status_code = response.status_code
    # ret_data = response.json

    # assert statements
    assert status_code == VALID_STATUS_CODE

    data = dict()
    data['parent_corp'] = 'SANOFI_CHC_2'
    data['global_top10'] = ['NESTLE, BAYER']
    data['competitor'] = 'CORP_2'
    response = client.post('/generate', content_type='multipart/form-data', data=data)
    assert response.status_code == VALID_STATUS_CODE
    assert response.data.decode("utf-8") == 'Pass'


@pytest.mark.generate
def test_route_generate_post_2(test_client: FlaskClient) -> None:
    """
    Testing the route '/generate' for the Value Driver app
    :param test_client: test client for this application
    :return: None
    """
    client = test_client
    # delete storage folder
    root_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    ip = client.environ_base['REMOTE_ADDR']
    ip_folder_path = os.path.join(root_dir, ip)
    import shutil
    shutil.rmtree(ip_folder_path, ignore_errors=True)

    frame = [('SANOFI_1', 'CAT_1'),
             ('COOOOOOOOOOOOOOOOOOOOOOOOOOOORP_2', 'CAT_1'),
             ('CORP_1', 'CAT_3'),
             ('SANOFI_CHC_22222222222222222222222222', 'CAT_2'),
             ('CORP_1', 'CAT_4'),
             ('CORP_3', 'CAT_3'),
             ('CORP_4', 'CAT_1'),
             ('CORP_4', 'CAT_4'),
             ('CORP_5', 'CAT_5'),
             ]
    d_frame = main.pd.DataFrame(frame, columns=['Corporation', 'Category'])
    d_frame['Country'] = 'IND'
    d_frame['Brand_Family'] = 'T'
    d_frame[['Val_PY', 'Val_CY', 'Vol_PY', 'Vol_CY']] = 0
    main.df = d_frame
    towrite = io.BytesIO()
    d_frame.to_excel(towrite, index=False)
    data = dict(file=(io.BytesIO(towrite.getvalue()), "temp_file.xlsx"))
    response = client.post('/process', data=data, follow_redirects=True,
                           content_type='multipart/form-data')
    status_code = response.status_code
    ret_data = response.json

    # assert statements
    assert status_code == VALID_STATUS_CODE
    assert ret_data['parent_orgs'] == [['', 'SANOFI_1'], ['selected', 'SANOFI_CHC_22222222222222222222222222']]
    comps_list = ['CORP_1', 'COOOOOOOOOOOOOOOOOOOOOOOOOOOORP_2', 'CORP_3', 'CORP_4', 'CORP_5']
    comps_list = sorted(map(lambda x: ['', x], comps_list))
    assert ret_data['non_parent_orgs'] == comps_list
    assert set(ret_data['global_top10']) == set()
    assert set(ret_data['non_global_top10']) == {'SANOFI_1', 'SANOFI_CHC_22222222222222222222222222', 'CORP_1', 'COOOOOOOOOOOOOOOOOOOOOOOOOOOORP_2', 'CORP_3', 'CORP_4',
                                                 'CORP_5'}
    assert set(ret_data['entry_orgs']) == {'SANOFI_CHC_22222222222222222222222222'}

    # start generating after processing
    data = dict()
    data['parent_corp'] = 'SANOFI_CHC_22222222222222222222222222'
    data['global_top10'] = []
    data['competitor'] = 'COOOOOOOOOOOOOOOOOOOOOOOOOOOORP_2'
    response = client.post('/generate', content_type='multipart/form-data', data=data)
    assert response.status_code == VALID_STATUS_CODE
    assert response.data.decode("utf-8") == 'Pass'


@pytest.mark.download_file
def test_route_download_file(test_client: FlaskClient) -> None:
    """
    Testing the route '/download_file' for the Value Driver app
    :param test_client: test client for this application
    :return: None
    """
    client = test_client
    response = client.get('/download_file')
    assert response.status_code == VALID_STATUS_CODE
    assert response.data


def test_is_online(test_client: FlaskClient) -> None:
    client = test_client
    response = client.get('/is_online')
    assert response.data.decode("utf-8") == 'Online'
