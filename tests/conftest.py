import pytest
from flask.testing import FlaskClient


@pytest.fixture
def test_client() -> FlaskClient:
    """
    Creates a test client for this application
    :return: FlaskClient
    """
    from main import app
    client = app.test_client()
    return client
