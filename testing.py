# import Value_Drivers as vd
#
# f = vd.ValueDriver('INPUT_FILE.xlsx')
# f.generate_report(
#    [0, 1, 2, 3, 4, 5, 6]
ls = [1, 2, 3, 4, 1, 2, 5]
sum_val = 7
res = []
pairs = []


def get_pairs(array, balance=0, pos=0):
    global pairs
    for i, num_i in enumerate(array, start=pos):
        c = (num_i + balance)
        if c == 7:
            pairs.append(i)
            res.append(pairs.copy())
            pairs.pop()
            c -= num_i
        elif c < 7:
            pairs.append(i)
            get_pairs(ls[i + 1:], balance=c, pos=(i + 1))
        else:
            c -= num_i
    if pairs:
        pairs.pop()


get_pairs(ls)

print(res)
