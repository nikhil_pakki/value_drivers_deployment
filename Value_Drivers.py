import os
import re
import warnings
import numpy as np
import pandas as pd

warnings.filterwarnings('ignore')

TAB_COLOR = '#00B050'
NEGATIVE_COLOR = '#FF0000'
START_POS_ROW = 3
START_POS_COL = 2
GAP_SIZE = 3
global_top10_tup = ('GSK + PFIZER', 'SANOFI', 'BAYER', 'JOHNSON', 'PROCTOR',
                    'RECKITT', 'TEVA', 'NESTLE', 'STADA', 'TAISHO')
global_top10_list = []
competitor = ''
corp = ''
corp_name = ''


def divide(n, d):
    if d:
        return n / d
    return 0


class ValueDriver:
    def __init__(self, filename, parent_corp, global_top10=[], competitor_name=None):
        global global_top10_list, competitor, corp
        self.TAB_COLOR = '#00B050'
        self.NEGATIVE_COLOR = '#FF0000'
        self.START_POS_ROW = 3
        self.START_POS_COL = 2
        self.GAP_SIZE = 3
        self.parent_corp = parent_corp
        corp = parent_corp
        self.global_top10_list = global_top10
        global_top10_list = global_top10
        # self.global_top10_list = ['GSK + Pfizer', 'Sanofi', 'Bayer', 'Johnson', 'Proctor',
        #                           'Reckitt', 'Teva', 'Nestle', 'Stada', 'Taisho']
        self.filename = filename
        self.sheetname = 'Sheet1'
        self.competitor = competitor_name
        competitor = competitor_name

    def generate_report(self, output_path=''):
        global global_top10_list, competitor, corp
        main(input_file=self.filename, sheet_name=self.sheetname, output_path=output_path)


def main(input_file: str, sheet_name: str = 'Sheet1', output_path: str = '') -> None:
    global global_top10_list, competitor, corp, corp_name
    corp_name = corp.title()
    if len(corp_name) >= 20:
        corp_name = corp_name[:20]
    df_sheet_list = []
    hide = False
    try:
        df = pd.read_excel(input_file, sheet_name=sheet_name)
    except ValueError as e:
        print(e)
        df = pd.read_excel(input_file)
    country = df['Country'].unique()[0]
    output_file_path = os.path.join(output_path, 'Value_Drivers.xlsx')
    writer = pd.ExcelWriter(output_file_path, engine='xlsxwriter')
    workbook = writer.book
    print('********************************')
    print('Reading input file ...')
    cat_columns = df.select_dtypes(exclude=[float, int]).columns.tolist()
    num_columns = df.select_dtypes(include=[float, int]).columns.tolist()
    df[cat_columns] = df[cat_columns].astype(str)
    df[cat_columns] = df[cat_columns].fillna('')
    df[num_columns] = df[num_columns].fillna(np.float64(0))
    df[num_columns] = df[num_columns].astype(np.float64)
    # df[num_columns] = round(df[num_columns], 10)

    # Entry Market
    print('Calculating Entry Market')

    # Entry_Markets calculation
    # get 'Category' list for which sanofi is part of
    # sanofi_cat_list = df[['Corporation', 'Category']][df['Corporation'].str.contains('sanofi', regex=True, case=False)]
    sanofi_cat_list = df[['Corporation', 'Category']][df['Corporation'] == corp]
    sanofi_cat_list = sanofi_cat_list['Category'].unique()
    df['Entry_Markets'] = df['Category'].map(lambda x: 1 if x in sanofi_cat_list else 0)

    # NPD New Brand
    # Filter on Excel Pivot, using Brand Family instead of Packs, and then consider YTD_PY=0, YTD_CY>0
    brand_pivot = df.groupby(['Brand_Family'])[['Val_PY', 'Val_CY']].agg('sum')
    brands = brand_pivot[(brand_pivot['Val_PY'] == np.float64(0)) & (brand_pivot['Val_CY'] > 0)]
    brand_lst = brands.index.to_list()
    df['NPD_NewBrand'] = df['Brand_Family'].isin(brand_lst)
    df['NPD_NewBrand'] = df['NPD_NewBrand'].astype(int)

    # Country Top 10
    country_top_10 = df.groupby(['Country', 'Corporation'])[['Val_PY', 'Val_CY']].agg('sum')
    country_top_10 = country_top_10.nlargest(10, 'Val_CY')
    top_10_corp_list = country_top_10.reset_index()['Corporation'].to_list()
    df['Country_Top10'] = df['Corporation'].isin(top_10_corp_list)
    df['Country_Top10'] = df['Country_Top10'].astype(int)

    # Global Top 10
    if global_top10_list:
        global_top10_list_regex = list(map(lambda x: '^' + re.sub(r'\W', r'.*', x, re.I) + '$', global_top10_list))
        global_top10_list_regex = '|'.join(global_top10_list_regex)
        df['Global_Top10'] = df['Corporation'].str.contains(global_top10_list_regex, regex=True, case=False)
        df['Global_Top10'] = df['Global_Top10'].astype(int)
    else:
        df['Global_Top10'] = 0

    # NPD VAL and NPD VOL
    print('Calculating NPD VAL and NPD VOL')
    df['NPD_VAL'] = np.float64((df['Val_PY'] == np.float64(0)) & (df['Val_CY'] > np.float64(0))) * df['Val_CY']
    df['NPD_VOL'] = np.float64((df['Vol_PY'] == np.float64(0)) & (df['Vol_CY'] > np.float64(0))) * df['Vol_CY']

    # Delisted VAL and Delisted VOL
    print('Calculating delisted VAL and delisted VOL')
    df['delisted_VAL'] = np.float64((df['Val_CY'] == np.float64(0)) & (df['Val_PY'] > np.float64(0))) * df['Val_PY']
    df['delisted_VOL'] = np.float64((df['Vol_CY'] == np.float64(0)) & (df['Vol_PY'] > np.float64(0))) * df['Vol_PY']

    # VOL Base and VAL Base (PY/CY)
    print('Calculating VOL Base and VAL Base (PY/CY)')
    df['VOL_Base_PY'] = np.float64((df['NPD_VOL'] == np.float64(0)) & (df['delisted_VOL'] == np.float64(0))) * df[
        'Vol_PY']
    df['VOL_Base_CY'] = np.float64((df['NPD_VOL'] == np.float64(0)) & (df['delisted_VOL'] == np.float64(0))) * df[
        'Vol_CY']
    df['VAL_Base_PY'] = np.float64((df['NPD_VAL'] == np.float64(0)) & (df['delisted_VAL'] == np.float64(0))) * df[
        'Val_PY']
    df['VAL_Base_CY'] = np.float64((df['NPD_VAL'] == np.float64(0)) & (df['delisted_VAL'] == np.float64(0))) * df[
        'Val_CY']

    # Price (PY/CY)
    print('Calculating Price (PY/CY)')
    df['Price_PY'] = df['VAL_Base_PY'] / df['VOL_Base_PY']
    df['Price_CY'] = df['VAL_Base_CY'] / df['VOL_Base_CY']

    # Price Effect
    print('Calculating Price Effect')
    df['Price_Effect'] = (df['Price_CY'] - df['Price_PY']) * df['VOL_Base_PY']

    # Pure Volume
    print('Calculating Pure Volume')
    df['Pure_Volume'] = (df['VOL_Base_CY'] - df['VOL_Base_PY']) * df['Price_PY']

    # PriceMix
    print('Calculating Price Mix')
    df['Price_Mix'] = (df['VOL_Base_CY'] - df['VOL_Base_PY']) * (df['Price_CY'] - df['Price_PY'])

    # cleaning output data
    numeric_columns = df._get_numeric_data().columns.tolist()
    df[numeric_columns] = df[numeric_columns].replace([np.inf, -np.inf, np.nan, -np.nan], np.float64(0))
    df[numeric_columns] = df[numeric_columns].fillna(np.float64(0))
    # df[numeric_columns] = df[numeric_columns].where(df[numeric_columns] == -np.float64(0), np.float64(0))
    df[numeric_columns] = round(df[numeric_columns], 2)

    # saving the output file
    print('Saving output file')

    # header and cell formatting and styling
    heading_format_style = {'bold': True,
                            'text_wrap': True,
                            'bg_color': 'yellow',
                            'border': 1,
                            'font_size': 10,
                            'align': 'center'}
    cell_format_style = {'align': 'centre',
                         'valign': 'centre',
                         'border': 1,
                         'font_size': 10}
    header_format = workbook.add_format(heading_format_style)
    cell_format = workbook.add_format(cell_format_style)
    font_size_alignment_format = workbook.add_format({'align': 'centre', 'valign': 'centre', 'font_size': 10})
    percent_format = workbook.add_format({'num_format': '0.00%'})

    df_sheet_list.append((df, 'Data Metrics', 0, 0, False))
    df.to_excel(writer, sheet_name='Data Metrics', index=False)

    # # Write the column headers with the defined format.
    # for col_num, value in enumerate(df.columns.values):
    #     writer.sheets['Data Metrics'].write(0, col_num, value, header_format)

    # Total Market Sheet
    hide = True
    start_row, start_col = START_POS_ROW, START_POS_COL
    total_market_cols = ['Country', 'Vol_PY', 'Vol_CY', 'Val_PY', 'Val_CY', 'delisted_VAL', 'delisted_VOL', 'NPD_VAL',
                         'Price_Effect', 'Pure_Volume', 'Price_Mix', 'NPD_New Product']
    total_market_df = df.copy()
    total_market_df['NPD_New Product'] = total_market_df['NPD_NewBrand'] * total_market_df['NPD_VAL']
    total_market_df = total_market_df.groupby('Country').agg('sum').reset_index()
    total_market_df = total_market_df[total_market_cols]
    # total_market_df['NPD_New Product'] = sum(df['NPD_NewBrand'] * df['NPD_VAL'])
    total_market_df.to_excel(writer, sheet_name='Total Market', index=False, startrow=start_row, startcol=start_col)
    df_sheet_list.append((total_market_df, 'Total Market', start_row, start_col, hide))

    # growth metrics for Total Market
    start_row += total_market_df.shape[0] + GAP_SIZE
    total_market_growth_metrics = total_market_df[['Country']]
    total_market_growth_metrics['Value Growth'] = (total_market_df['Val_CY'] / total_market_df['Val_PY']) - 1
    total_market_growth_metrics['Pure Volume Growth'] = (total_market_df['Pure_Volume'] / total_market_df['Val_PY'])
    total_market_growth_metrics['Delisted Growth'] = -(total_market_df['delisted_VAL'] / total_market_df['Val_PY'])
    total_market_growth_metrics['Pure Price Growth'] = (total_market_df['Price_Effect'] / total_market_df['Val_PY'])
    total_market_growth_metrics['Mix Growth'] = (total_market_df['Price_Mix'] / total_market_df['Val_PY'])
    total_market_growth_metrics['Incremental Growth'] = 0
    total_market_growth_metrics['NPD_New_Brands Growth'] = (
                total_market_df['NPD_New Product'] / total_market_df['Val_PY'])
    total_market_growth_metrics['Incremental Growth'] = (total_market_df['NPD_VAL'] / total_market_df['Val_PY'])
    total_market_growth_metrics['Incremental Growth'] -= total_market_growth_metrics['NPD_New_Brands Growth']
    numeric_columns = total_market_growth_metrics._get_numeric_data().columns.tolist()
    total_market_growth_metrics[numeric_columns] = total_market_growth_metrics[numeric_columns].replace(
        [np.inf, -np.inf, np.nan, -np.nan], np.float64(0))
    # total_market_growth_metrics[numeric_columns] = total_market_growth_metrics[numeric_columns].applymap('{:.2%}'.format)
    total_market_growth_metrics.to_excel(writer, sheet_name='Total Market', index=False, startrow=start_row,
                                         startcol=start_col)
    df_sheet_list.append((total_market_growth_metrics, 'Total Market', start_row, start_col, hide))

    # Entry Market
    hide = True
    start_row, start_col = START_POS_ROW, START_POS_COL
    entry_market_df_filter = df[df['Entry_Markets'] == 1]
    entry_market_df_filter['NPD_New Product'] = entry_market_df_filter['NPD_NewBrand'] * entry_market_df_filter[
        'NPD_VAL']
    entry_market_cols = ['Country', 'Vol_PY', 'Vol_CY', 'Val_PY', 'Val_CY', 'delisted_VAL', 'delisted_VOL', 'NPD_VAL',
                         'Price_Effect', 'Pure_Volume', 'Price_Mix', 'NPD_New Product']
    entry_market_df = entry_market_df_filter.groupby('Country').agg('sum').reset_index()
    entry_market_df = entry_market_df[entry_market_cols]
    entry_market_df['NPD_New Product'] = sum(entry_market_df_filter['NPD_NewBrand'] * entry_market_df_filter['NPD_VAL'])
    # entry_market_df.to_excel(writer, sheet_name='Entry Market', index=False, startrow=start_row, startcol=start_col)
    # df_sheet_list.append((entry_market_df, 'Entry Market', start_row, start_col, hide))

    # growth metrics for Entry Market
    # start_row += entry_market_df.shape[0] + GAP_SIZE
    entry_market_growth_metrics = entry_market_df[['Country']]
    entry_market_growth_metrics['Value Growth'] = (entry_market_df['Val_CY'] / entry_market_df['Val_PY']) - 1
    entry_market_growth_metrics['Pure Volume Growth'] = (entry_market_df['Pure_Volume'] / entry_market_df['Val_PY'])
    entry_market_growth_metrics['Delisted Growth'] = -(entry_market_df['delisted_VAL'] / entry_market_df['Val_PY'])
    entry_market_growth_metrics['Pure Price Growth'] = (entry_market_df['Price_Effect'] / entry_market_df['Val_PY'])
    entry_market_growth_metrics['Mix Growth'] = (entry_market_df['Price_Mix'] / entry_market_df['Val_PY'])
    entry_market_growth_metrics['Incremental Growth'] = 0
    entry_market_growth_metrics['NPD_New_Brands Growth'] = (
                entry_market_df['NPD_New Product'] / entry_market_df['Val_PY'])
    entry_market_growth_metrics['Incremental Growth'] = (entry_market_df['NPD_VAL'] / entry_market_df['Val_PY'])
    entry_market_growth_metrics['Incremental Growth'] -= entry_market_growth_metrics['NPD_New_Brands Growth']
    numeric_columns = entry_market_growth_metrics._get_numeric_data().columns.tolist()
    entry_market_growth_metrics[numeric_columns] = entry_market_growth_metrics[numeric_columns].replace(
        [np.inf, -np.inf, np.nan, -np.nan], np.float64(0))
    # entry_market_growth_metrics[numeric_columns] = entry_market_growth_metrics[numeric_columns].applymap('{:.2%}'.format)
    entry_market_growth_metrics.to_excel(writer, sheet_name='Entry Market', index=False, startrow=start_row,
                                         startcol=start_col)
    df_sheet_list.append((entry_market_growth_metrics, 'Entry Market', start_row, start_col, hide))

    # Focus Entry Markets by Category
    # start_row += entry_market_growth_metrics.shape[0] + GAP_SIZE
    focus_entry_market_df_filter = df[df['Entry_Markets'] == 1]
    focus_entry_market_df_filter['NPD_New Product'] = focus_entry_market_df_filter['NPD_NewBrand'] * \
                                                      focus_entry_market_df_filter['NPD_VAL']
    focus_entry_market_cols = ['Category', 'Vol_PY', 'Vol_CY', 'Val_PY', 'Val_CY', 'delisted_VAL', 'delisted_VOL',
                               'NPD_VAL', 'Price_Effect', 'Pure_Volume', 'Price_Mix', 'NPD_New Product']
    focus_entry_market_df = focus_entry_market_df_filter.groupby(['Country', 'Category']).agg('sum').reset_index()
    focus_entry_market_df = focus_entry_market_df[focus_entry_market_cols]
    # focus_entry_market_df.to_excel(writer, sheet_name='Entry Market', index=False, startrow=start_row, startcol=start_col)
    # df_sheet_list.append((focus_entry_market_df, 'Entry Market', start_row, start_col, hide))

    # growth metrics for focus entry Market
    start_row += entry_market_growth_metrics.shape[0] + GAP_SIZE
    focus_entry_market_growth_metrics = focus_entry_market_df[['Category']].copy()  # pd.DataFrame({})
    focus_entry_market_growth_metrics['Value Growth'] = (focus_entry_market_df['Val_CY'] / focus_entry_market_df[
        'Val_PY']) - 1
    focus_entry_market_growth_metrics['Pure Volume Growth'] = (
                focus_entry_market_df['Pure_Volume'] / focus_entry_market_df['Val_PY'])
    focus_entry_market_growth_metrics['Delisted Growth'] = -(
                focus_entry_market_df['delisted_VAL'] / focus_entry_market_df['Val_PY'])
    focus_entry_market_growth_metrics['Pure Price Growth'] = (
                focus_entry_market_df['Price_Effect'] / focus_entry_market_df['Val_PY'])
    focus_entry_market_growth_metrics['Mix Growth'] = (
                focus_entry_market_df['Price_Mix'] / focus_entry_market_df['Val_PY'])
    focus_entry_market_growth_metrics['Incremental Growth'] = 0
    focus_entry_market_growth_metrics['NPD_New_Brands Growth'] = (
                focus_entry_market_df['NPD_New Product'] / focus_entry_market_df['Val_PY'])
    focus_entry_market_growth_metrics['Incremental Growth'] = (
                focus_entry_market_df['NPD_VAL'] / focus_entry_market_df['Val_PY'])
    focus_entry_market_growth_metrics['Incremental Growth'] -= focus_entry_market_growth_metrics[
        'NPD_New_Brands Growth']
    numeric_columns = focus_entry_market_growth_metrics._get_numeric_data().columns.tolist()
    focus_entry_market_growth_metrics[numeric_columns] = focus_entry_market_growth_metrics[numeric_columns].replace(
        [np.inf, -np.inf, np.nan, -np.nan], np.float64(0))
    # focus_entry_market_growth_metrics[numeric_columns] = focus_entry_market_growth_metrics[numeric_columns].applymap('{:.2%}'.format)
    focus_entry_market_growth_metrics.to_excel(writer, sheet_name='Entry Market', index=False, startrow=start_row,
                                               startcol=start_col)
    df_sheet_list.append((focus_entry_market_growth_metrics, 'Entry Market', start_row, start_col, hide))

    # sanofi market sheet
    hide = True
    start_row, start_col = START_POS_ROW, START_POS_COL
    # sanofi_market_df_filter = df[df['Corporation'].str.contains('sanofi', regex=True, case=False)]
    sanofi_market_df_filter = df[df['Corporation'] == corp]
    sanofi_market_df_filter['NPD_New Product'] = sanofi_market_df_filter['NPD_NewBrand'] * sanofi_market_df_filter[
        'NPD_VAL']
    sanofi_market_cols = ['Country', 'Vol_PY', 'Vol_CY', 'Val_PY', 'Val_CY', 'delisted_VAL', 'delisted_VOL', 'NPD_VAL',
                          'Price_Effect', 'Pure_Volume', 'Price_Mix', 'NPD_New Product']
    sanofi_market_df = sanofi_market_df_filter.groupby('Country').agg('sum').reset_index()
    sanofi_market_df = sanofi_market_df[sanofi_market_cols]
    # sanofi_market_df.to_excel(writer, sheet_name=f'{corp_name} Market', index=False, startrow=start_row, startcol=start_col)
    # df_sheet_list.append((sanofi_market_df, f'{corp_name} Market', start_row, start_col, hide))

    # growth metrics for Sanofi Market
    # start_row += sanofi_market_df.shape[0] + GAP_SIZE
    sanofi_market_growth_metrics = sanofi_market_df[['Country']]  # pd.DataFrame({})
    sanofi_market_growth_metrics['Value Growth'] = (sanofi_market_df['Val_CY'] / sanofi_market_df['Val_PY']) - 1
    sanofi_market_growth_metrics['Pure Volume Growth'] = (sanofi_market_df['Pure_Volume'] / sanofi_market_df['Val_PY'])
    sanofi_market_growth_metrics['Delisted Growth'] = -(sanofi_market_df['delisted_VAL'] / sanofi_market_df['Val_PY'])
    sanofi_market_growth_metrics['Pure Price Growth'] = (sanofi_market_df['Price_Effect'] / sanofi_market_df['Val_PY'])
    sanofi_market_growth_metrics['Mix Growth'] = (sanofi_market_df['Price_Mix'] / sanofi_market_df['Val_PY'])
    sanofi_market_growth_metrics['Incremental Growth'] = 0
    sanofi_market_growth_metrics['NPD_New_Brands Growth'] = (
                sanofi_market_df['NPD_New Product'] / sanofi_market_df['Val_PY'])
    sanofi_market_growth_metrics['Incremental Growth'] = (sanofi_market_df['NPD_VAL'] / sanofi_market_df['Val_PY'])
    sanofi_market_growth_metrics['Incremental Growth'] -= sanofi_market_growth_metrics['NPD_New_Brands Growth']
    numeric_columns = sanofi_market_growth_metrics._get_numeric_data().columns.tolist()
    sanofi_market_growth_metrics[numeric_columns] = sanofi_market_growth_metrics[numeric_columns].replace(
        [np.inf, -np.inf, np.nan, -np.nan], np.float64(0))
    # sanofi_market_growth_metrics[numeric_columns] = sanofi_market_growth_metrics[numeric_columns].applymap('{:.2%}'.format)
    sanofi_market_growth_metrics.to_excel(writer, sheet_name=f'{corp_name} Market', index=False, startrow=start_row,
                                          startcol=start_col)
    df_sheet_list.append((sanofi_market_growth_metrics, f'{corp_name} Market', start_row, start_col, hide))

    # Sanofi Markets by Category
    # start_row += sanofi_market_growth_metrics.shape[0] + GAP_SIZE
    sanofi_category_df_filter = sanofi_market_df_filter.copy()
    sanofi_category_df_filter['NPD_New Product'] = sanofi_category_df_filter['NPD_NewBrand'] * \
                                                   sanofi_category_df_filter['NPD_VAL']
    sanofi_category_market_cols = ['Category', 'Vol_PY', 'Vol_CY', 'Val_PY', 'Val_CY', 'delisted_VAL', 'delisted_VOL',
                                   'NPD_VAL', 'Price_Effect', 'Pure_Volume', 'Price_Mix', 'NPD_New Product']
    sanofi_category_df = sanofi_category_df_filter.groupby(['Country', 'Category']).agg('sum').reset_index()
    sanofi_category_df = sanofi_category_df[sanofi_category_market_cols]
    # sanofi_category_df.to_excel(writer, sheet_name=f'{corp_name} Market', index=False, startrow=start_row, startcol=start_col)
    # df_sheet_list.append((sanofi_category_df, f'{corp_name} Market', start_row, start_col, hide))

    # growth metrics for Sanofi Markets by Category
    start_row += sanofi_market_growth_metrics.shape[0] + GAP_SIZE
    sanofi_category_growth_metrics = sanofi_category_df[['Category']].copy()  # pd.DataFrame({})
    sanofi_category_growth_metrics['Value Growth'] = (sanofi_category_df['Val_CY'] / sanofi_category_df[
        'Val_PY']) - 1
    sanofi_category_growth_metrics['Pure Volume Growth'] = (
            sanofi_category_df['Pure_Volume'] / sanofi_category_df['Val_PY'])
    sanofi_category_growth_metrics['Delisted Growth'] = -(
            sanofi_category_df['delisted_VAL'] / sanofi_category_df['Val_PY'])
    sanofi_category_growth_metrics['Pure Price Growth'] = (
            sanofi_category_df['Price_Effect'] / sanofi_category_df['Val_PY'])
    sanofi_category_growth_metrics['Mix Growth'] = (
            sanofi_category_df['Price_Mix'] / sanofi_category_df['Val_PY'])
    sanofi_category_growth_metrics['Incremental Growth'] = 0
    sanofi_category_growth_metrics['NPD_New_Brands Growth'] = (
            sanofi_category_df['NPD_New Product'] / sanofi_category_df['Val_PY'])
    sanofi_category_growth_metrics['Incremental Growth'] = (
            sanofi_category_df['NPD_VAL'] / sanofi_category_df['Val_PY'])
    sanofi_category_growth_metrics['Incremental Growth'] -= sanofi_category_growth_metrics[
        'NPD_New_Brands Growth']
    numeric_columns = sanofi_category_growth_metrics._get_numeric_data().columns.tolist()
    sanofi_category_growth_metrics[numeric_columns] = sanofi_category_growth_metrics[numeric_columns].replace(
        [np.inf, -np.inf, np.nan, -np.nan], np.float64(0))
    # sanofi_category_growth_metrics[numeric_columns] = sanofi_category_growth_metrics[numeric_columns].applymap(
    #     '{:.2%}'.format)
    sanofi_category_growth_metrics.to_excel(writer, sheet_name=f'{corp_name} Market', index=False, startrow=start_row,
                                            startcol=start_col)
    df_sheet_list.append((sanofi_category_growth_metrics, f'{corp_name} Market', start_row, start_col, hide))

    # SANOFI BRANDS
    hide = True
    start_row, start_col = START_POS_ROW, START_POS_COL
    sanofi_brands_df = sanofi_market_df_filter.copy()
    sanofi_brands_df['NPD_New Product'] = sanofi_brands_df['NPD_NewBrand'] * sanofi_brands_df['NPD_VAL']
    sanofi_brands_cols = ['Brand_Family', 'Vol_PY', 'Vol_CY', 'Val_PY', 'Val_CY', 'delisted_VAL',
                          'delisted_VOL', 'NPD_VAL', 'Price_Effect', 'Pure_Volume', 'Price_Mix', 'NPD_New Product']
    sanofi_brands_df = sanofi_brands_df.groupby(['Brand_Family']).agg('sum').reset_index()
    sanofi_brands_df = sanofi_brands_df[sanofi_brands_cols]
    # sanofi_brands_df.to_excel(writer, sheet_name=f'{corp_name} Brands', index=False, startrow=start_row, startcol=start_col)
    # df_sheet_list.append((sanofi_brands_df, f'{corp_name} Brands', start_row, start_col, hide))

    # growth metrics for Sanofi brands
    # start_row += sanofi_brands_df.shape[0] + GAP_SIZE
    sanofi_brands_growth_metrics = sanofi_brands_df[['Brand_Family']].copy()  # pd.DataFrame({})
    sanofi_brands_growth_metrics['Value Growth'] = (sanofi_brands_df['Val_CY'] / sanofi_brands_df['Val_PY']) - 1
    sanofi_brands_growth_metrics['Pure Volume Growth'] = (sanofi_brands_df['Pure_Volume'] / sanofi_brands_df['Val_PY'])
    sanofi_brands_growth_metrics['Delisted Growth'] = -(sanofi_brands_df['delisted_VAL'] / sanofi_brands_df['Val_PY'])
    sanofi_brands_growth_metrics['Pure Price Growth'] = (sanofi_brands_df['Price_Effect'] / sanofi_brands_df['Val_PY'])
    sanofi_brands_growth_metrics['Mix Growth'] = (sanofi_brands_df['Price_Mix'] / sanofi_brands_df['Val_PY'])
    sanofi_brands_growth_metrics['Incremental Growth'] = 0
    sanofi_brands_growth_metrics['NPD_New_Brands Growth'] = (
                sanofi_brands_df['NPD_New Product'] / sanofi_brands_df['Val_PY'])
    sanofi_brands_growth_metrics['Incremental Growth'] = (sanofi_brands_df['NPD_VAL'] / sanofi_brands_df['Val_PY'])
    sanofi_brands_growth_metrics['Incremental Growth'] -= sanofi_brands_growth_metrics['NPD_New_Brands Growth']
    sanofi_brands_growth_metrics['Change in absolute price'] = ((sanofi_brands_df['Val_CY'] /
                                                                 sanofi_brands_df['Vol_CY']) /
                                                                (sanofi_brands_df['Val_PY'] /
                                                                 sanofi_brands_df['Vol_PY'])) - 1
    numeric_columns = sanofi_brands_growth_metrics._get_numeric_data().columns.tolist()
    sanofi_brands_growth_metrics[numeric_columns] = sanofi_brands_growth_metrics[numeric_columns].replace(
        [np.inf, -np.inf, np.nan, -np.nan], np.float64(0))
    sanofi_brands_growth_metrics.to_excel(writer, sheet_name=f'{corp_name} Brands', index=False, startrow=start_row,
                                          startcol=start_col)
    df_sheet_list.append((sanofi_brands_growth_metrics, f'{corp_name} Brands', start_row, start_col, hide))

    # COUNTRY TOP 10
    # competitor analysis
    hide = False
    start_row, start_col = START_POS_ROW, START_POS_COL
    country_top_10_filter = df[df['Country_Top10'] == 1].copy()
    country_top_10_filter['NPD_New Product'] = country_top_10_filter['NPD_NewBrand'] * country_top_10_filter['NPD_VAL']
    country_top_10_cols = ['Country', 'Corporation', 'Vol_PY', 'Vol_CY', 'Val_PY', 'Val_CY', 'delisted_VAL',
                           'delisted_VOL', 'NPD_VAL', 'Price_Effect', 'Pure_Volume', 'Price_Mix', 'NPD_New Product']
    country_top_10_df = country_top_10_filter.groupby(['Country', 'Corporation']).agg('sum').reset_index()
    country_top_10_df = country_top_10_df[country_top_10_cols]
    # country_top_10_df.to_excel(writer, sheet_name='Country Top 10', index=False, startrow=start_row, startcol=start_col)
    # df_sheet_list.append((country_top_10_df, 'Country Top 10', start_row, start_col, hide))

    # growth metrics for country top 10
    # start_row += country_top_10_df.shape[0] + GAP_SIZE
    country_top_10_growth_metrics = country_top_10_df[['Country', 'Corporation']].copy()  # pd.DataFrame({})
    country_top_10_growth_metrics['Value Growth'] = (country_top_10_df['Val_CY'] / country_top_10_df['Val_PY']) - 1
    country_top_10_growth_metrics['Pure Volume Growth'] = (
            country_top_10_df['Pure_Volume'] / country_top_10_df['Val_PY'])
    country_top_10_growth_metrics['Delisted Growth'] = -(
            country_top_10_df['delisted_VAL'] / country_top_10_df['Val_PY'])
    country_top_10_growth_metrics['Pure Price Growth'] = (
            country_top_10_df['Price_Effect'] / country_top_10_df['Val_PY'])
    country_top_10_growth_metrics['Mix Growth'] = (
            country_top_10_df['Price_Mix'] / country_top_10_df['Val_PY'])
    country_top_10_growth_metrics['Incremental Growth'] = 0
    country_top_10_growth_metrics['NPD_New_Brands Growth'] = (
            country_top_10_df['NPD_New Product'] / country_top_10_df['Val_PY'])
    country_top_10_growth_metrics['Incremental Growth'] = (
            country_top_10_df['NPD_VAL'] / country_top_10_df['Val_PY'])
    country_top_10_growth_metrics['Incremental Growth'] -= country_top_10_growth_metrics[
        'NPD_New_Brands Growth']
    numeric_columns = country_top_10_growth_metrics._get_numeric_data().columns.tolist()
    country_top_10_growth_metrics[numeric_columns] = country_top_10_growth_metrics[numeric_columns].replace(
        [np.inf, -np.inf, np.nan, -np.nan], np.float64(0))
    # country_top_10_growth_metrics[numeric_columns] = country_top_10_growth_metrics[numeric_columns].applymap(
    #     '{:.2%}'.format)
    country_top_10_growth_metrics.to_excel(writer, sheet_name='Country Top 10', index=False, startrow=start_row,
                                           startcol=start_col)
    df_sheet_list.append((country_top_10_growth_metrics, 'Country Top 10', start_row, start_col, hide))

    if global_top10_list:
        # GLOBAL TOP 10
        # competitor analysis
        hide = False
        start_row, start_col = START_POS_ROW, START_POS_COL
        global_top_10_filter = df[df['Global_Top10'] == 1].copy()
        global_top_10_filter['NPD_New Product'] = global_top_10_filter['NPD_NewBrand'] * global_top_10_filter['NPD_VAL']
        country_top_10_cols = ['Country', 'Corporation', 'Vol_PY', 'Vol_CY', 'Val_PY', 'Val_CY', 'delisted_VAL',
                               'delisted_VOL', 'NPD_VAL', 'Price_Effect', 'Pure_Volume', 'Price_Mix', 'NPD_New Product']
        global_top_10_df = global_top_10_filter.groupby(['Country', 'Corporation']).agg('sum').reset_index()
        global_top_10_df = global_top_10_df[country_top_10_cols]
        # global_top_10_df.to_excel(writer, sheet_name='Global Top 10', index=False, startrow=start_row,
        #                           startcol=start_col)
        # df_sheet_list.append((global_top_10_df, 'Global Top 10', start_row, start_col, hide))

        # growth metrics for country top 10
        # start_row += global_top_10_df.shape[0] + GAP_SIZE
        global_top_10_growth_metrics = global_top_10_df[['Country', 'Corporation']].copy()  # pd.DataFrame({})
        global_top_10_growth_metrics['Value Growth'] = (global_top_10_df['Val_CY'] / global_top_10_df['Val_PY']) - 1
        global_top_10_growth_metrics['Pure Volume Growth'] = (
                global_top_10_df['Pure_Volume'] / global_top_10_df['Val_PY'])
        global_top_10_growth_metrics['Delisted Growth'] = -(
                global_top_10_df['delisted_VAL'] / global_top_10_df['Val_PY'])
        global_top_10_growth_metrics['Pure Price Growth'] = (
                global_top_10_df['Price_Effect'] / global_top_10_df['Val_PY'])
        global_top_10_growth_metrics['Mix Growth'] = (
                global_top_10_df['Price_Mix'] / global_top_10_df['Val_PY'])
        global_top_10_growth_metrics['Incremental Growth'] = 0
        global_top_10_growth_metrics['NPD_New_Brands Growth'] = (
                    global_top_10_df['NPD_New Product'] / global_top_10_df['Val_PY'])
        global_top_10_growth_metrics['Incremental Growth'] = (global_top_10_df['NPD_VAL'] / global_top_10_df['Val_PY'])
        global_top_10_growth_metrics['Incremental Growth'] -= global_top_10_growth_metrics[
            'NPD_New_Brands Growth']
        numeric_columns = global_top_10_growth_metrics._get_numeric_data().columns.tolist()
        global_top_10_growth_metrics[numeric_columns] = global_top_10_growth_metrics[numeric_columns].replace(
            [np.inf, -np.inf, np.nan, -np.nan], np.float64(0))
        # global_top_10_growth_metrics[numeric_columns] = global_top_10_growth_metrics[numeric_columns].applymap(
        #     '{:.2%}'.format)
        global_top_10_growth_metrics.to_excel(writer, sheet_name='Global Top 10', index=False, startrow=start_row,
                                              startcol=start_col)
        df_sheet_list.append((global_top_10_growth_metrics, 'Global Top 10', start_row, start_col, hide))

    # Brand drivers
    hide = False
    start_row, start_col = START_POS_ROW, START_POS_COL
    if global_top10_list:
        entries = ['Total OTC Market Growth', 'Country Top 10', 'Global Top 10']
        sources = [total_market_df, country_top_10_df, global_top_10_df]
    else:
        entries = ['Total OTC Market Growth', 'Country Top 10']
        sources = [total_market_df, country_top_10_df]

    if not entry_market_df.empty:
        entries.append('Entry Market')
        sources.append(entry_market_df)
    if not sanofi_market_df.empty:
        entries.append(f'{corp_name} Market')
        sources.append(sanofi_market_df)

    metrics = ['PH', 'Value Growth', 'Pure Volume Growth', 'Delisted Effect', 'Pure Price',
               'Mix Effect', 'Incremental Launch Growth', 'New Brand Launch Effect']
    # metrics = {'Value Growth': 'Value Growth', 'Pure Volume Growth': 'Pure Volume Growth',
    #            'Delisted Effect': 'Delisted Growth', 'Pure Price': 'Pure Price Growth',
    #            'Mix Effect': 'Mix Growth', 'Incremental Launch Growth': 'Incremental Growth',
    #            'New Brand Launch Effect': 'NPD_New_Brands Growth'}
    data = []
    for i, source in enumerate(sources):
        rows = list()
        rows.append(entries[i])
        val_py = sum(source['Val_PY'])
        rows.append(divide(sum(source['Val_CY']), val_py) - 1)
        rows.append(divide(sum(source['Pure_Volume']), val_py))
        rows.append(divide(-sum(source['delisted_VAL']), val_py))
        rows.append(divide(sum(source['Price_Effect']), val_py))
        rows.append(divide(sum(source['Price_Mix']), val_py))
        rows.append(divide(sum(source['NPD_VAL'] - source['NPD_New Product']), val_py))
        rows.append(divide(sum(source['NPD_New Product']), val_py))
        data.append(rows)
    bd = pd.DataFrame(data, columns=metrics)
    # bd.set_index('PH', inplace=True)
    bd.to_excel(writer, sheet_name='Brand Drivers', index=False, startrow=start_row, startcol=start_col)
    df_sheet_list.append((bd, 'Brand Drivers', start_row, start_col, hide))

    sheet1 = writer.sheets['Brand Drivers']

    heading1_format_style = {'bold': True,
                             'text_wrap': True,
                             'align': 'center',
                             'valign': 'vcenter',
                             'bg_color': 'white',
                             'border': 1,
                             'font_size': 10}
    heading1_format = workbook.add_format(heading1_format_style)
    heading2_format_style = {'bold': True,
                             'bg_color': '#B7DEE8',
                             'border': 1,
                             'align': 'center',
                             'valign': 'vcenter',
                             'font_size': 10}
    heading2_format = workbook.add_format(heading2_format_style)
    # merge_format = workbook.add_format({'valign': 'center'})
    sheet1.merge_range(f'C{start_row}:C{start_row + 1}', country, heading2_format)
    sheet1.merge_range(f'D{start_row}:D{start_row + 1}', 'Value Growth', heading2_format)
    # sheet1.merge_range(f'D{start_row}:D{start_row+1}', 'Value Growth', merge_format)
    sheet1.merge_range(f'E{start_row}:F{start_row}', 'Core Growth', heading1_format)
    sheet1.merge_range(f'G{start_row}:H{start_row}', 'Price Growth', heading1_format)
    sheet1.merge_range(f'I{start_row}:J{start_row}', 'NPD Growth', heading1_format)

    # Focus Entry Market by Categories
    start_row += bd.shape[0] + GAP_SIZE
    entry_market_cat_brand = focus_entry_market_df_filter.copy()
    entry_market_cat_brand['NPD_New Product'] = entry_market_cat_brand['NPD_NewBrand'] * entry_market_cat_brand[
        'NPD_VAL']

    # changes as per siddharth
    entry_market_cat_brand_total = entry_market_cat_brand.copy()
    entry_market_cat_brand_total['Market'] = ' OVERALL '
    entry_market_cat_brand_total['Brand_Family'] = 'TOTAL'
    # entry_market_cat_brand_sanofi = entry_market_cat_brand[entry_market_cat_brand['Corporation'].str.contains('sanofi', regex=True, case=False)]
    entry_market_cat_brand_sanofi = entry_market_cat_brand[entry_market_cat_brand['Corporation'] == corp]
    entry_market_cat_brand_sanofi['Market'] = f'{corp_name}'
    entry_market_cat_brand = pd.concat([entry_market_cat_brand_total, entry_market_cat_brand_sanofi])
    # entry_market_cat_brand['Market'] = np.where(entry_market_cat_brand['Corporation'].str.contains('sanofi', regex=True, case=False), f'{corp_name}, 'EXTERNAL')
    # entry_market_cat_brand.loc[~entry_market_cat_brand['Corporation'].str.contains('sanofi', regex=True, case=False), 'Brand_Family'] = 'Others'

    entry_market_cat_brand = entry_market_cat_brand.groupby(['Category', 'Market', 'Brand_Family']).agg('sum')
    entry_market_cat_cols = ['Vol_PY', 'Vol_CY', 'Val_PY', 'Val_CY', 'delisted_VAL', 'delisted_VOL', 'NPD_VAL',
                             'Price_Effect', 'Pure_Volume', 'Price_Mix', 'NPD_New Product']
    entry_market_cat_brand = entry_market_cat_brand[entry_market_cat_cols]
    # entry_market_cat_brand.to_excel(writer, sheet_name='Brand Drivers', index=True, startrow=start_row, startcol=start_col)
    # df_sheet_list.append((entry_market_cat_brand, 'Brand Drivers', start_row, start_col+3))

    entry_market_cat_growth = entry_market_cat_brand.copy()  # pd.DataFrame({})
    entry_market_cat_growth['Value Growth'] = (entry_market_cat_growth['Val_CY'] / entry_market_cat_growth[
        'Val_PY']) - 1
    entry_market_cat_growth['Pure Volume Growth'] = (
            entry_market_cat_growth['Pure_Volume'] / entry_market_cat_growth['Val_PY'])
    entry_market_cat_growth['Delisted Growth'] = -(
            entry_market_cat_growth['delisted_VAL'] / entry_market_cat_growth['Val_PY'])
    entry_market_cat_growth['Pure Price Growth'] = (
            entry_market_cat_growth['Price_Effect'] / entry_market_cat_growth['Val_PY'])
    entry_market_cat_growth['Mix Growth'] = (
            entry_market_cat_growth['Price_Mix'] / entry_market_cat_growth['Val_PY'])
    entry_market_cat_growth['Incremental Growth'] = 0
    entry_market_cat_growth['NPD_New_Brands Growth'] = (
            entry_market_cat_growth['NPD_New Product'] / entry_market_cat_growth['Val_PY'])
    entry_market_cat_growth['Incremental Growth'] = (
            entry_market_cat_growth['NPD_VAL'] / entry_market_cat_growth['Val_PY'])
    entry_market_cat_growth['Incremental Growth'] -= entry_market_cat_growth[
        'NPD_New_Brands Growth']
    entry_market_cat_growth.drop(entry_market_cat_cols, inplace=True, axis=1)
    numeric_columns = entry_market_cat_growth._get_numeric_data().columns.tolist()
    entry_market_cat_growth[numeric_columns] = entry_market_cat_growth[numeric_columns].replace(
        [np.inf, -np.inf, np.nan, -np.nan], np.float64(0))
    if not entry_market_cat_growth.empty:
        entry_market_cat_growth.to_excel(writer, sheet_name='Brand Drivers', index=True, startrow=start_row,
                                         startcol=start_col)
        df_sheet_list.append((entry_market_cat_growth, 'Brand Drivers', start_row, start_col, hide))

    # competitor market sheet
    if competitor:
        competitor_sheet_name = competitor
        # sheet name limitation
        if len(competitor_sheet_name) >= 15:
            competitor_sheet_name = competitor_sheet_name[:15]

        # competitor market sheet
        hide = False
        start_row, start_col = START_POS_ROW, START_POS_COL
        competitor_market_df_filter = df[df['Corporation'].str.contains(competitor, regex=True, case=False)]
        competitor_market_df_filter['NPD_New Product'] = competitor_market_df_filter['NPD_NewBrand'] * \
                                                         competitor_market_df_filter['NPD_VAL']
        competitor_market_cols = ['Country', 'Vol_PY', 'Vol_CY', 'Val_PY', 'Val_CY', 'delisted_VAL', 'delisted_VOL',
                                  'NPD_VAL',
                                  'Price_Effect', 'Pure_Volume', 'Price_Mix', 'NPD_New Product']
        competitor_market_df = competitor_market_df_filter.groupby('Country').agg('sum').reset_index()
        competitor_market_df = competitor_market_df[competitor_market_cols]
        # competitor_market_df.to_excel(writer, sheet_name=f'{competitor_sheet_name} Market', index=False, startrow=start_row,
        #                           startcol=start_col)
        # df_sheet_list.append((competitor_market_df, f'{competitor_sheet_name} Market', start_row, start_col, hide))

        # growth metrics for competitor Market
        # start_row += competitor_market_df.shape[0] + GAP_SIZE
        competitor_market_growth_metrics = competitor_market_df[['Country']]  # pd.DataFrame({})
        competitor_market_growth_metrics['Value Growth'] = (competitor_market_df['Val_CY'] / competitor_market_df[
            'Val_PY']) - 1
        competitor_market_growth_metrics['Pure Volume Growth'] = (
                competitor_market_df['Pure_Volume'] / competitor_market_df['Val_PY'])
        competitor_market_growth_metrics['Delisted Growth'] = -(
                competitor_market_df['delisted_VAL'] / competitor_market_df['Val_PY'])
        competitor_market_growth_metrics['Pure Price Growth'] = (
                competitor_market_df['Price_Effect'] / competitor_market_df['Val_PY'])
        competitor_market_growth_metrics['Mix Growth'] = (
                    competitor_market_df['Price_Mix'] / competitor_market_df['Val_PY'])
        competitor_market_growth_metrics['Incremental Growth'] = 0
        competitor_market_growth_metrics['NPD_New_Brands Growth'] = (
                competitor_market_df['NPD_New Product'] / competitor_market_df['Val_PY'])
        competitor_market_growth_metrics['Incremental Growth'] = (
                competitor_market_df['NPD_VAL'] / competitor_market_df['Val_PY'])
        competitor_market_growth_metrics['Incremental Growth'] -= competitor_market_growth_metrics[
            'NPD_New_Brands Growth']
        numeric_columns = competitor_market_growth_metrics._get_numeric_data().columns.tolist()
        competitor_market_growth_metrics[numeric_columns] = competitor_market_growth_metrics[numeric_columns].replace(
            [np.inf, -np.inf, np.nan, -np.nan], np.float64(0))
        # competitor_market_growth_metrics[numeric_columns] = competitor_market_growth_metrics[numeric_columns].applymap('{:.2%}'.format)
        competitor_market_growth_metrics.to_excel(writer, sheet_name=f'{competitor_sheet_name} Market', index=False,
                                                  startrow=start_row,
                                                  startcol=start_col)
        df_sheet_list.append(
            (competitor_market_growth_metrics, f'{competitor_sheet_name} Market', start_row, start_col, hide))

        # competitor brand wise
        start_row += competitor_market_growth_metrics.shape[0] + GAP_SIZE
        # start_row, start_col = START_POS_ROW, START_POS_COL
        competitor_market_df_filter = df[df['Corporation'].str.contains(competitor, regex=True, case=False)]
        competitor_market_df_filter['NPD_New Product'] = competitor_market_df_filter['NPD_NewBrand'] * \
                                                         competitor_market_df_filter['NPD_VAL']

        # competitor Markets by Category
        competitor_category_df_filter = competitor_market_df_filter.copy()
        competitor_category_df_filter['NPD_New Product'] = competitor_category_df_filter['NPD_NewBrand'] * \
                                                           competitor_category_df_filter['NPD_VAL']
        competitor_category_market_cols = ['Category', 'Vol_PY', 'Vol_CY', 'Val_PY', 'Val_CY', 'delisted_VAL',
                                           'delisted_VOL', 'NPD_VAL', 'Price_Effect', 'Pure_Volume', 'Price_Mix',
                                           'NPD_New Product']
        competitor_category_df = competitor_category_df_filter.groupby(['Country', 'Category']).agg('sum').reset_index()
        competitor_category_df = competitor_category_df[competitor_category_market_cols]
        # competitor_category_df.to_excel(writer, sheet_name=f'{competitor_sheet_name} Market', index=False, startrow=start_row,
        #                             startcol=start_col)
        # df_sheet_list.append((competitor_category_df, f'{competitor_sheet_name} Market', start_row, start_col, hide))

        # growth metrics for Competitor Markets by Category
        # start_row += competitor_category_df.shape[0] + GAP_SIZE
        competitor_category_growth_metrics = competitor_category_df[['Category']].copy()  # pd.DataFrame({})
        competitor_category_growth_metrics['Value Growth'] = (competitor_category_df['Val_CY'] / competitor_category_df[
            'Val_PY']) - 1
        competitor_category_growth_metrics['Pure Volume Growth'] = (
                competitor_category_df['Pure_Volume'] / competitor_category_df['Val_PY'])
        competitor_category_growth_metrics['Delisted Growth'] = -(
                competitor_category_df['delisted_VAL'] / competitor_category_df['Val_PY'])
        competitor_category_growth_metrics['Pure Price Growth'] = (
                competitor_category_df['Price_Effect'] / competitor_category_df['Val_PY'])
        competitor_category_growth_metrics['Mix Growth'] = (
                competitor_category_df['Price_Mix'] / competitor_category_df['Val_PY'])
        competitor_category_growth_metrics['Incremental Growth'] = 0
        competitor_category_growth_metrics['NPD_New_Brands Growth'] = (
                competitor_category_df['NPD_New Product'] / competitor_category_df['Val_PY'])
        competitor_category_growth_metrics['Incremental Growth'] = (
                competitor_category_df['NPD_VAL'] / competitor_category_df['Val_PY'])
        competitor_category_growth_metrics['Incremental Growth'] -= competitor_category_growth_metrics[
            'NPD_New_Brands Growth']
        numeric_columns = competitor_category_growth_metrics._get_numeric_data().columns.tolist()
        competitor_category_growth_metrics[numeric_columns] = competitor_category_growth_metrics[
            numeric_columns].replace(
            [np.inf, -np.inf, np.nan, -np.nan], np.float64(0))
        competitor_category_growth_metrics.to_excel(writer, sheet_name=f'{competitor_sheet_name} Market', index=False,
                                                    startrow=start_row,
                                                    startcol=start_col)
        df_sheet_list.append(
            (competitor_category_growth_metrics, f'{competitor_sheet_name} Market', start_row, start_col, hide))

        # Competitor BRANDS
        start_row += competitor_category_growth_metrics.shape[0] + GAP_SIZE
        competitor_brands_df = competitor_market_df_filter.copy()
        competitor_brands_df['NPD_New Product'] = competitor_brands_df['NPD_NewBrand'] * competitor_brands_df['NPD_VAL']
        competitor_brands_cols = ['Brand_Family', 'Vol_PY', 'Vol_CY', 'Val_PY', 'Val_CY', 'delisted_VAL',
                                  'delisted_VOL', 'NPD_VAL', 'Price_Effect', 'Pure_Volume', 'Price_Mix',
                                  'NPD_New Product']
        competitor_brands_df = competitor_brands_df.groupby(['Brand_Family']).agg('sum').reset_index()
        competitor_brands_df = competitor_brands_df[competitor_brands_cols]
        # competitor_brands_df.to_excel(writer, sheet_name=f'{competitor_sheet_name} Market', index=False, startrow=start_row,
        #                           startcol=start_col)
        # df_sheet_list.append((competitor_brands_df, f'{competitor_sheet_name} Market', start_row, start_col, hide))

        # growth metrics for competitor brands
        # start_row += competitor_brands_df.shape[0] + GAP_SIZE
        competitor_brands_growth_metrics = competitor_brands_df[['Brand_Family']].copy()  # pd.DataFrame({})
        competitor_brands_growth_metrics['Value Growth'] = (competitor_brands_df['Val_CY'] / competitor_brands_df[
            'Val_PY']) - 1
        competitor_brands_growth_metrics['Pure Volume Growth'] = (
                competitor_brands_df['Pure_Volume'] / competitor_brands_df['Val_PY'])
        competitor_brands_growth_metrics['Delisted Growth'] = -(
                competitor_brands_df['delisted_VAL'] / competitor_brands_df['Val_PY'])
        competitor_brands_growth_metrics['Pure Price Growth'] = (
                competitor_brands_df['Price_Effect'] / competitor_brands_df['Val_PY'])
        competitor_brands_growth_metrics['Mix Growth'] = (
                    competitor_brands_df['Price_Mix'] / competitor_brands_df['Val_PY'])
        competitor_brands_growth_metrics['Incremental Growth'] = 0
        competitor_brands_growth_metrics['NPD_New_Brands Growth'] = (
                competitor_brands_df['NPD_New Product'] / competitor_brands_df['Val_PY'])
        competitor_brands_growth_metrics['Incremental Growth'] = (
                    competitor_brands_df['NPD_VAL'] / competitor_brands_df['Val_PY'])
        competitor_brands_growth_metrics['Incremental Growth'] -= competitor_brands_growth_metrics[
            'NPD_New_Brands Growth']
        competitor_brands_growth_metrics['Change in absolute price'] = ((competitor_brands_df['Val_CY'] /
                                                                         competitor_brands_df['Vol_CY']) /
                                                                        (competitor_brands_df['Val_PY'] /
                                                                         competitor_brands_df['Vol_PY'])) - 1
        numeric_columns = competitor_brands_growth_metrics._get_numeric_data().columns.tolist()
        competitor_brands_growth_metrics[numeric_columns] = competitor_brands_growth_metrics[numeric_columns].replace(
            [np.inf, -np.inf, np.nan, -np.nan], np.float64(0))
        competitor_brands_growth_metrics.to_excel(writer, sheet_name=f'{competitor_sheet_name} Market', index=False,
                                                  startrow=start_row, startcol=start_col)
        df_sheet_list.append(
            (competitor_brands_growth_metrics, f'{competitor_sheet_name} Market', start_row, start_col, hide))

    # Overall Analysis
    if competitor:
        hide = False
        start_row, start_col = START_POS_ROW, START_POS_COL
        overall_analysis_temp = focus_entry_market_df_filter.copy()
        overall_analysis_temp['Market'] = ' OVERALL '
        overall_analysis_temp['Brand_Family'] = 'TOTAL'
        overall_analysis_sanofi = focus_entry_market_df_filter.copy()
        # overall_analysis_sanofi = overall_analysis_sanofi[overall_analysis_sanofi['Corporation'].str.contains('sanofi', regex=True, case=False)]
        overall_analysis_sanofi = overall_analysis_sanofi[overall_analysis_sanofi['Corporation'] == corp]
        overall_analysis_sanofi['Market'] = f'{corp_name}'
        overall_analysis_concat = pd.concat([overall_analysis_temp, overall_analysis_sanofi])
        overall_analysis_comp = focus_entry_market_df_filter.copy()
        overall_analysis_comp = overall_analysis_comp[
            overall_analysis_comp['Corporation'].str.contains(competitor, regex=True, case=False)]
        overall_analysis_comp['Market'] = competitor

        overall_analysis_cols = ['Vol_PY', 'Vol_CY', 'Val_PY', 'Val_CY', 'delisted_VAL', 'delisted_VOL', 'NPD_VAL',
                                 'Price_Effect', 'Pure_Volume', 'Price_Mix', 'NPD_New Product']
        # overall_analysis_df = sanofi_temp.groupby(['Category']).agg('sum')
        # overall_analysis_df.reset_index(inplace=True)
        # overall_analysis_df['Market'] = f'{corp_name}'
        # overall_analysis_df['Brand_Family'] = ' TOTAL '
        overall_analysis_df = pd.concat([overall_analysis_concat, overall_analysis_comp])
        overall_analysis_df = overall_analysis_df.groupby(['Category', 'Market', 'Brand_Family']).agg('sum').sort_index(
            level=0)
        overall_analysis_df = overall_analysis_df[overall_analysis_cols]
        # overall_analysis_df.to_excel(writer, sheet_name='Overall Analysis', index=True, startrow=start_row, startcol=start_col)
        # df_sheet_list.append((overall_analysis_df, 'Overall Analysis', start_row, start_col, hide))

        # overall growth metrics
        # start_row += overall_analysis_df.shape[0] + GAP_SIZE
        overall_growth = overall_analysis_df.copy()  # pd.DataFrame({})
        overall_growth['Value Growth'] = (overall_growth['Val_CY'] / overall_growth['Val_PY']) - 1
        overall_growth['Pure Volume Growth'] = (overall_growth['Pure_Volume'] / overall_growth['Val_PY'])
        overall_growth['Delisted Growth'] = -(overall_growth['delisted_VAL'] / overall_growth['Val_PY'])
        overall_growth['Pure Price Growth'] = (overall_growth['Price_Effect'] / overall_growth['Val_PY'])
        overall_growth['Mix Growth'] = (overall_growth['Price_Mix'] / overall_growth['Val_PY'])
        overall_growth['Incremental Growth'] = 0
        overall_growth['NPD_New_Brands Growth'] = (overall_growth['NPD_New Product'] / overall_growth['Val_PY'])
        overall_growth['Incremental Growth'] = (overall_growth['NPD_VAL'] / overall_growth['Val_PY'])
        overall_growth['Incremental Growth'] -= overall_growth['NPD_New_Brands Growth']
        overall_growth.drop(overall_analysis_cols, inplace=True, axis=1)
        numeric_columns = overall_growth._get_numeric_data().columns.tolist()
        overall_growth[numeric_columns] = overall_growth[numeric_columns].replace(
            [np.inf, -np.inf, np.nan, -np.nan], np.float64(0))
        if not overall_growth.empty:
            overall_growth.to_excel(writer, sheet_name='Overall Analysis', index=True, startrow=start_row,
                                    startcol=start_col)
            df_sheet_list.append((overall_growth, 'Overall Analysis', start_row, start_col, hide))

    # format the data and write to excel
    for dataframe, sheetname, startrow, startcolumn, hide in df_sheet_list:
        # adding tab color
        sheet = writer.sheets[sheetname]
        sheet.set_tab_color(TAB_COLOR)
        r, c = dataframe.shape
        ml_c = 0
        if isinstance(dataframe.index, pd.MultiIndex):
            ml_c = len(dataframe.index[0])
        cols = dataframe.columns
        percent_col_index = 0
        for i, col in enumerate(cols, start=1):
            if 'Growth' in col:
                percent_col_index = i + 1
                break
        if percent_col_index:
            sheet.conditional_format(first_row=startrow, first_col=percent_col_index + ml_c,
                                     last_row=(startrow + r), last_col=(percent_col_index + c + ml_c),
                                     options={'type': 'no_blanks', 'format': percent_format})
            sheet.conditional_format(first_row=startrow, first_col=percent_col_index + ml_c,
                                     last_row=(startrow + r), last_col=(percent_col_index + c + ml_c),
                                     options={'type': 'data_bar', 'bar_color': TAB_COLOR,
                                              'bar_negative_color': NEGATIVE_COLOR, 'min_value': 0, 'max_value': 100})
        sheet.conditional_format(first_row=startrow, first_col=startcolumn,
                                 last_row=(startrow + r), last_col=(startcolumn + c + ml_c),
                                 options={'type': 'no_blanks', 'format': cell_format})
        for col_num, column in enumerate(dataframe.columns.values):
            # format headers
            sheet.write(startrow, col_num + startcolumn + ml_c, column, header_format)
            # fit the rows and columns
            column_length = max(dataframe[column].astype(str).map(len).max(), len(column)) + 1
            col_idx = dataframe.columns.get_loc(column)
            sheet.set_column(col_idx + startcolumn + ml_c, col_idx + startcolumn, column_length,
                             font_size_alignment_format)
            # sheet.conditional_format(first_row=startrow, first_col=startcolumn,
            #                          last_row=(startrow + r), last_col=(startcolumn + c),
            #                          options={'type': 'no_blanks', 'format': cell_format})

        if hide:
            sheet.hide()

    writer.save()

    print('********************************')
    return


if __name__ == '__main__':
    main('INPUT_FILE.xlsx')
